package id.ac.ub.filkom_195150401111021.papb_c_rest_api.Model_C_195150401111021;

//import com.google.gson.annotations.SerializedName;

public class Perpustakaan {
    private String id;
    private String judul;
    private String deskripsi;

    public Perpustakaan(){}

    public Perpustakaan(String id, String judul, String deskripsi) {
        this.id = id;
        this.judul = judul;
        this.deskripsi = deskripsi;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }
}

