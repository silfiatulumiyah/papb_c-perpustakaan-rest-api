package id.ac.ub.filkom_195150401111021.papb_c_rest_api;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import id.ac.ub.filkom_195150401111021.papb_c_rest_api.Model_C_195150401111021.PostPutDelPerpustakaan;
import id.ac.ub.filkom_195150401111021.papb_c_rest_api.Rest_C_195150401111021.ApiClient;
import id.ac.ub.filkom_195150401111021.papb_c_rest_api.Rest_C_195150401111021.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
public class Insert extends AppCompatActivity {
    EditText etJudul, etDeskripsi;
    Button btInsert, btBack;
    ApiInterface mApiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert);
        etJudul = (EditText) findViewById(R.id.etJudul);
        etDeskripsi = (EditText) findViewById(R.id.etDeskripsi);
        mApiInterface = ApiClient.getClient().create(ApiInterface.class);
        btInsert = (Button) findViewById(R.id.btInserting);
        btInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Call<PostPutDelPerpustakaan> postPerpustakaaanCall = mApiInterface.postPerpustakaan(etJudul.getText().toString(), etDeskripsi.getText().toString());
                postPerpustakaaanCall.enqueue(new Callback<PostPutDelPerpustakaan>() {
                    @Override
                    public void onResponse(Call<PostPutDelPerpustakaan> call, Response<PostPutDelPerpustakaan> response) {
                        MainActivity.ma.refresh();
                        finish();
                    }

                    @Override
                    public void onFailure(Call<PostPutDelPerpustakaan> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

        btBack = (Button) findViewById(R.id.btBackGo);
        btBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.ma.refresh();
                finish();
            }
        });
    }
}