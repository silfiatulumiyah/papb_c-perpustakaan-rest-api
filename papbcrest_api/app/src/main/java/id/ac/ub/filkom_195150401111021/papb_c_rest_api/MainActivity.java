package id.ac.ub.filkom_195150401111021.papb_c_rest_api;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import retrofit2.Callback;

import java.util.List;

import id.ac.ub.filkom_195150401111021.papb_c_rest_api.Adapter_C_195150401111021.PerpustakaanAdapter;
import id.ac.ub.filkom_195150401111021.papb_c_rest_api.Model_C_195150401111021.GetPerpustakaan;
import id.ac.ub.filkom_195150401111021.papb_c_rest_api.Model_C_195150401111021.Perpustakaan;
import id.ac.ub.filkom_195150401111021.papb_c_rest_api.Rest_C_195150401111021.ApiClient;
import id.ac.ub.filkom_195150401111021.papb_c_rest_api.Rest_C_195150401111021.ApiInterface;
import retrofit2.Call;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    Button bt;
    ApiInterface mApiInterface;
    private RecyclerView mRecyclerView;
    private PerpustakaanAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    public static MainActivity ma;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bt = (Button) findViewById(R.id.bt);
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, Insert.class));
            }
        });
//        mRecyclerView = (RecyclerView) findViewById(R.id.RecyclerView);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mApiInterface = ApiClient.getClient().create(ApiInterface.class);
        ma = this;
        refresh();
    }

    public void refresh() {
        Call<GetPerpustakaan> perpustakaanCall = mApiInterface.getPerpustakaan();
        perpustakaanCall.enqueue(new Callback<GetPerpustakaan>() {
            @Override
            public void onResponse(Call<GetPerpustakaan> call, Response<GetPerpustakaan>
                    response) {
                List<Perpustakaan> PerpustakaanList = response.body().getListDataPerpustakaan();
                Log.d("Retrofit Get", "Jumlah data Buku: " +
                        String.valueOf(PerpustakaanList.size()));
                mAdapter = new PerpustakaanAdapter(PerpustakaanList);
                mRecyclerView.setAdapter(mAdapter);
            }

            @Override
            public void onFailure(Call<GetPerpustakaan> call, Throwable t) {
                Log.e("Retrofit Get", t.toString());
            }
        });
    }
}