package id.ac.ub.filkom_195150401111021.papb_c_rest_api.Rest_C_195150401111021;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {


        public static final String BASE_URL = "http://192.168.56.1/ci-papb-restserver/index.php/";
        private static Retrofit retrofit = null;
        public static Retrofit getClient() {
            if (retrofit==null) {
                retrofit = new Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
            }
            return retrofit;
        }
    }

