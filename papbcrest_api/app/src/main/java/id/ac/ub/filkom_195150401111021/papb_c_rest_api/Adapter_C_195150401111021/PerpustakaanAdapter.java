package id.ac.ub.filkom_195150401111021.papb_c_rest_api.Adapter_C_195150401111021;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import id.ac.ub.filkom_195150401111021.papb_c_rest_api.Edit;
import id.ac.ub.filkom_195150401111021.papb_c_rest_api.Model_C_195150401111021.Perpustakaan;
import id.ac.ub.filkom_195150401111021.papb_c_rest_api.R;

public class PerpustakaanAdapter extends RecyclerView.Adapter<PerpustakaanAdapter.ViewHolder> {
    LayoutInflater inflatter;
    List<Perpustakaan> mPerpustakaanList;

    public PerpustakaanAdapter(Context context, List<Perpustakaan> mPerpustakaanList ) {
        this.inflatter = LayoutInflater.from(context);
        this.mPerpustakaanList = mPerpustakaanList;
    }

    public PerpustakaanAdapter(List<Perpustakaan> perpustakaanList) {
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = inflatter.inflate(R.layout.activity_insert, parent, false);
        return new ViewHolder(itemView);
    }
    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvJudul;
        TextView tvDeskripsi;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvJudul = itemView.findViewById(R.id.tvJudul);
            tvDeskripsi = itemView.findViewById(R.id.tvDeskripsi);
        }
    }
    @Override
    public void onBindViewHolder(@NonNull PerpustakaanAdapter.ViewHolder holder, int position) {
        Perpustakaan item = mPerpustakaanList.get(position);
        holder.tvJudul.setText(item.getJudul());
        holder.tvDeskripsi.setText(item.getDeskripsi());
    }

    @Override
    public int getItemCount () {
        return mPerpustakaanList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvId, tvJudul, tvDeskripsi;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvJudul = (TextView) itemView.findViewById(R.id.tvJudul);
            tvDeskripsi = (TextView) itemView.findViewById(R.id.tvDeskripsi);
        }
    }
    public void onBindViewHolder (MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        holder.tvId.setText("Id = " + mPerpustakaanList.get(position).getId());
        holder.tvJudul.setText("Judul = " + mPerpustakaanList.get(position).getJudul());
        holder.tvDeskripsi.setText("Deskripsi = " + mPerpustakaanList.get(position).getDeskripsi());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent = new Intent(view.getContext(), Edit.class);
                mIntent.putExtra("Id", mPerpustakaanList.get(position).getId());
                mIntent.putExtra("Judul", mPerpustakaanList.get(position).getJudul());
                mIntent.putExtra("Deskripsi", mPerpustakaanList.get(position).getDeskripsi());
                view.getContext().startActivity(mIntent);
            }
        });
    }
}
