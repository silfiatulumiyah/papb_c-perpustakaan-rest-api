package id.ac.ub.filkom_195150401111021.papb_c_rest_api.Model_C_195150401111021;

public class PostPutDelPerpustakaan {
    String status;
    Perpustakaan mPerpustakaan;
    String message;
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    public Perpustakaan getPerpustakaan() {
        return mPerpustakaan;
    }
    public void setPerpustakaan(Perpustakaan Perpustakaan) {
        mPerpustakaan = Perpustakaan;
    }

}

