package id.ac.ub.filkom_195150401111021.papb_c_rest_api.Model_C_195150401111021;
import com.google.gson.annotations.SerializedName;

import java.util.List;
public class GetPerpustakaan {
    @SerializedName("status")
    String status;
    @SerializedName("result")
    List<Perpustakaan> listDataPerpustakaan;
    @SerializedName("message")
    String message;
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    public List<Perpustakaan> getListDataPerpustakaan() {
        return listDataPerpustakaan;
    }
    public void setListDataPerpustakaan(List<Perpustakaan> listDataKontak) {
        this.listDataPerpustakaan = listDataPerpustakaan;
    }
}


