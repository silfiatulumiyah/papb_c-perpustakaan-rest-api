package id.ac.ub.filkom_195150401111021.papb_c_rest_api.Rest_C_195150401111021;

import id.ac.ub.filkom_195150401111021.papb_c_rest_api.Model_C_195150401111021.GetPerpustakaan;
import id.ac.ub.filkom_195150401111021.papb_c_rest_api.Model_C_195150401111021.PostPutDelPerpustakaan;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.POST;
import retrofit2.http.PUT;

public interface ApiInterface {
    @GET("PerpustakaanPAPB_android")
    Call<GetPerpustakaan> getPerpustakaan();
    @FormUrlEncoded
    @POST("perpustakaan")
    Call<PostPutDelPerpustakaan> postPerpustakaan(@Field("judul") String judul,
                                      @Field("deskripsi") String deskripsi);
    @FormUrlEncoded
    @PUT("perpustakaan")
    Call<PostPutDelPerpustakaan> putPerpustakaan(@Field("id") String id,
                                     @Field("judul") String judul,
                                     @Field("deskripsi") String deskripsi);
    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "perpustakaan", hasBody = true)
    Call<PostPutDelPerpustakaan> deletePerpustakaan(@Field("id") String id);
}

